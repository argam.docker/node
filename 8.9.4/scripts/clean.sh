#!/bin/sh

APP_NAME='base-node-8.6.0'

TAG='dev'

docker stop $APP_NAME || true

docker rm $APP_NAME || true

docker rmi $APP_NAME:$TAG || true